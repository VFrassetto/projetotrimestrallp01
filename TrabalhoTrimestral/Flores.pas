unit Flores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Imaging.jpeg,
  Vcl.ExtCtrls;

type
  TFlor = class(TForm)
    BotaoAdd: TButton;
    BotaoAtt: TButton;
    BotaoDelete: TButton;
    BotaoLimpa: TButton;
    BotaoSalva: TButton;
    BotaoCarregar: TButton;
    Lista1: TListBox;
    Ed_Flor: TEdit;
    Ed_Cor: TEdit;
    Ed_Tamanho: TEdit;
    Ed_Edita: TEdit;
    TextoGrande: TLabel;
    ImagemFlor: TImage;
    procedure BotaoAddClick(Sender: TObject);
    procedure BotaoDeleteClick(Sender: TObject);
    procedure BotaoLimpaClick(Sender: TObject);
    procedure BotaoAttClick(Sender: TObject);
    procedure BotaoCarregarClick(Sender: TObject);
    procedure BotaoSalvaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Flor: TFlor;
  arquivo : TextFile;
  i :integer;

implementation

{$R *.dfm}

procedure TFlor.BotaoAddClick(Sender: TObject);
begin
Lista1.Items.Add(Ed_Flor.text);
Lista1.Items.Add(Ed_Cor.text);
Lista1.Items.Add(Ed_Tamanho.text);
end;

procedure TFlor.BotaoAttClick(Sender: TObject);
begin
Lista1.Items[Lista1.itemindex] := Ed_Edita.text;
end;

procedure TFlor.BotaoCarregarClick(Sender: TObject);
begin
Lista1.Items.LoadfromFile(' Flores.txt ');
end;

procedure TFlor.BotaoDeleteClick(Sender: TObject);
begin
Lista1.DeleteSelected;
end;

procedure TFlor.BotaoLimpaClick(Sender: TObject);
begin
Lista1.Clear;
end;

procedure TFlor.BotaoSalvaClick(Sender: TObject);
var
  i: Integer;
begin
AssignFile(arquivo, ' Flores.txt ');
Rewrite(arquivo);
begin for i := 0 to Lista1.Items.Count-1 do
WriteLn(arquivo,Lista1.Items.Strings[i]);
end;
CloseFile(arquivo);
end;

end.
